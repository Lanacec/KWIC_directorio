import Kommander.MasterKommander;

import java.io.File;
import java.util.Scanner;

public class Main {
    public static void main (String args[]) {
        Scanner leer = new Scanner(System.in);
        String key;
        String dir;
        System.out.println("Ingrese la URL del directorio donde desea buscar los ficheros con las palabras clave");
        dir=leer.nextLine();
        if ( !new File(dir).isDirectory()){
            System.out.println("La URL no es un directorio");
            return;
        }
        System.out.println("Ingrese la URL del archivo donde estan las palabras clave");
        key=leer.nextLine();
        if ( !new File(key).isFile()){
            System.out.println("La URL no es un archivo");
            return;
        }
        MasterKommander masterKommander = new MasterKommander();
        masterKommander.process(dir,key);
    }
}

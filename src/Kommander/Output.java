package Kommander;

import com.sun.tools.doclets.formats.html.SourceToHTMLConverter;

public class Output {
private Context context;

    public Output(Context context) {
        this.context = context;
    }

    public void run(){
        for (int i = 0; i < context.guardapares.size() ; i++){
            Context.Pairof par = context.guardapares.get(i);
            System.out.print(par.keyw + ": ");
            for(int j = 0; j < par.miarray.size(); j++) {
                if ( j == par.miarray.size() - 1 ) {
                    System.out.print(par.miarray.get(j));
                } else {
                    System.out.print(par.miarray.get(j) + ", ");
                }
            }

            System.out.println();
        }


    }
}

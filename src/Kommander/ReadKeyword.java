package Kommander;

public class ReadKeyword {
    private Context context;

    public ReadKeyword (Context context){
        this.context=context;
    }

    public void process(){

        context.keyword = new String(context.chars).split("\n");
    }

}

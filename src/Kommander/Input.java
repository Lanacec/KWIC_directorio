package Kommander;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Input {
    private Context context;

    public Input(Context context) {
        this.context = context;
    }

    public void run(String fileName) {

        try {
            byte[] content = Files.readAllBytes(Paths.get(fileName));

            context.chars = new char[content.length];
            for(int i = 0; i < content.length; i++) {
                context.chars[i] = (char)content[i];
            }

        } catch (IOException e) {
        }


    }
}


package Kommander;


import java.util.ArrayList;

public class LookForWords {
    private Context context;
    ArrayList<String> miarray;


    public LookForWords(Context context){
        this.context=context;
    }

    public void process(){

        // programacion
        // de
        // tecnicas

        context.guardapares = new ArrayList();

        for (int i = 0; i < context.keyword.length; i++){
            String keyword = context.keyword[i].trim();

            if ( keyword.isEmpty() ) {
                continue;
            }

            miarray = new ArrayList();

            // Valida si contiene keyword con cada archivo del directorio
            for (int j = 0; j < context.directorio.size(); j++) {

                String directorio = context.directorio.get(j);
                String arreglo[] = directorio.split(" ");

                for (int x = 0; x < arreglo.length; x++) {
                    String word = arreglo[x];

                    int indexOfDot = word.lastIndexOf(".");
                    if ( indexOfDot != -1 ) {
                        word = word.substring(0, indexOfDot);
                    }

                    if ( word.equals(keyword) &&  ! miarray.contains(directorio)) {
                        miarray.add(directorio);
                    }
                }

            }

            Context.Pairof par = new Context.Pairof(keyword, miarray);
            context.guardapares.add(par);

        }





    }


}

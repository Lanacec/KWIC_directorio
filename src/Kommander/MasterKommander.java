package Kommander;

public class MasterKommander {

    private Input input;
    private Context context;
    private ReadKeyword readkey;
    private ReadFilesDirectory readdir;
    private Output output;
    private LookForWords lookfw;

    public MasterKommander() {
        context = new Context();
        input = new Input(context);
        readkey = new ReadKeyword(context);
        readdir = new ReadFilesDirectory(context);
        output = new Output(context);
        lookfw = new LookForWords(context);

    }

    public void process(String directoryName, String keywordFilename) {
        input.run(keywordFilename);
        readkey.process();
        readdir.process(directoryName);
        lookfw.process();
        output.run();


    }
}